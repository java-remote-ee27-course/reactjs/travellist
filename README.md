# Travel list demo app

App for creating your travel list, using form in ReactJS.

# Description

Compose your travel list:

- Add items to the list
- Remove items from the list (X)
- Clear the whole list (warning message is displayed before the full clear)
- Mark items to be packed (checkboxes)
- Sort items by:
  - input order
  - added by
  - description A-Y
  - description Y-A
  - packed first
  - packed last
- Statistics
- Different notices in statistics, based your travel list
- (Travel list is initialized with some provisional data, which can be removed by clicking X icons after each item)

![travel list1](./public/assets/travellist2.png)

### Sort by description:

![travel list2](./public/assets/travellist3.png)
![travel list3](./public/assets/travellist4.png)

### Sort by packed status:

![travel list4](./public/assets/travellist5.png)
![travel list5](./public/assets/travellist6.png)

### Different messages in statistics section below

![travel list6](./public/assets/travellist8.png)
![travel list7](./public/assets/travellist9.png)

## Prerequisites to run the code

Coming soon
