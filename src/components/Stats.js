function Stats({ items }) {
  if (items.length === 0) {
    return (
      <footer className="stats">
        <div>You have no items in the list, start adding them!</div>
      </footer>
    );
  }

  const numberOfItems = items.length;
  const packedCount = items.filter((item) => item.packed).length;
  const percentageOfItemsPacked = Math.round(
    (packedCount / numberOfItems) * 100
  );

  return (
    <footer className="stats">
      <div>
        {percentageOfItemsPacked === 100
          ? `Everything packed. You are ready to go!`
          : `You have ${numberOfItems} items in your list, 
            and ${packedCount} items (${percentageOfItemsPacked}%) packed.`}
      </div>
    </footer>
  );
}
export default Stats;
