import { useState } from "react";

import Form from "./Form";
import Stats from "./Stats";
import PackingList from "./PackingList";
import Logo from "./Logo";
//starts at 69

const initialItems = [
  { id: 1, description: "Umbrellas", quantity: 1, packed: false },
  { id: 2, description: "Sunglasses", quantity: 3, packed: false },
  { id: 3, description: "Swimming-suites", quantity: 4, packed: true },
  { id: 4, description: "Hats", quantity: 2, packed: false },
  { id: 5, description: "Towels", quantity: 3, packed: false },
  { id: 6, description: "Shampoo", quantity: 1, packed: false },
];

function App() {
  const [items, setItems] = useState(initialItems);

  function handleAddItems(item) {
    setItems((items) => [...items, item]);
  }

  function handleDeleteItem(id) {
    setItems((items) => items.filter((item) => item.id !== id));
  }

  function handleToggleItem(id) {
    // use existing useState, create a new array,
    // if item.id to be checked === id, we spread its fields and change the "packed" value,
    // and w else do not change, just leave the item as it is
    setItems((items) =>
      items.map((item) =>
        item.id === id ? { ...item, packed: !item.packed } : item
      )
    );
  }
  function handleClearAll() {
    const confirmDelete = window.confirm(
      "Are you sure you want to delete all items from the list?"
    );
    confirmDelete && setItems([]);
  }

  return (
    <div className="app">
      <Logo />
      <Form onAddItems={handleAddItems} />
      <PackingList
        items={items}
        onDeleteItem={handleDeleteItem}
        onToggleItem={handleToggleItem}
        onClearAll={handleClearAll}
      />
      <Stats items={items} />
    </div>
  );
}

export default App;
