import { useState } from "react";
import Item from "./Item.js";

const sortFilter = [
  "Sort by input order",
  "Sort by description A-Y",
  "Sort by description Y-A",
  "Sort by packed first",
  "Sort by packed last",
];

function PackingList({ items, onDeleteItem, onToggleItem, onClearAll }) {
  const [sortBy, setSortBy] = useState(sortFilter[0]);
  let sortedItems;

  if (sortBy === sortFilter[0]) {
    sortedItems = items;
  }

  if (sortBy === sortFilter[1]) {
    sortedItems = items
      .slice()
      .sort((item1, item2) =>
        item1.description.localeCompare(item2.description)
      );
  }

  if (sortBy === sortFilter[2]) {
    sortedItems = items
      .slice()
      .sort((item1, item2) =>
        item2.description.localeCompare(item1.description)
      );
  }

  if (sortBy === sortFilter[3]) {
    sortedItems = items
      .slice()
      .sort((item1, item2) => Number(item2.packed) - Number(item1.packed));
  }

  if (sortBy === sortFilter[4]) {
    sortedItems = items
      .slice()
      .sort((item1, item2) => Number(item1.packed) - Number(item2.packed));
  }

  return (
    <div className="list">
      <ul>
        {sortedItems.map((item) => (
          <Item
            key={item.id}
            item={item}
            onDeleteItem={onDeleteItem}
            onPackItem={onToggleItem}
          />
        ))}
      </ul>

      <div className="actions">
        <select value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
          {Array.from(sortFilter).map((item) => (
            <option value={item} key={item}>
              {item}
            </option>
          ))}
        </select>
        <button onClick={onClearAll}>Clear all</button>
      </div>
    </div>
  );
}

export default PackingList;
